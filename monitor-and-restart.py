#! /usr/bin/env python

# Standard library imports
import os

# Third party library imports
from openstack import connection
from openstack import profile
from openstack import exceptions

prof = profile.Profile()
prof.set_version('image', 'v2')

OPENSTACK_RC = {'profile': prof,
                'auth_url': os.environ['OS_AUTH_URL'],
                'username': os.environ['OS_USERNAME'],
                'password': os.environ['OS_PASSWORD'],
                'project_name': os.environ['OS_PROJECT_NAME'],
                'user_domain_name': os.environ['OS_USER_DOMAIN_NAME'],
                'project_domain_name': os.environ['OS_PROJECT_DOMAIN_NAME'],
                }

conn = connection.Connection(**OPENSTACK_RC)
nova = conn.compute

for server in nova.servers():
    if server.status == 'SHUTOFF':
        print("Server : " + server.name + " found shutoff.  Starting it")
        nova.start_server(server)
        nova.wait_for_server
        print("Server : " + server.name + " restarted")
    time.sleep(1)